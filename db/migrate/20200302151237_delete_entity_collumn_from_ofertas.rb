class DeleteEntityCollumnFromOfertas < ActiveRecord::Migration[6.0]
  def change
    remove_column :oferta, :entity
    add_index :oferta, [:entidade_id, :created_at]
    rename_table :oferta, :ofertas
  end
end
