class AddPageToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :page, :string
  end
end
