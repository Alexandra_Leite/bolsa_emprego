class RenameTableOferta < ActiveRecord::Migration[6.0]
  def change
    rename_table :ofertas, :oferta
  end
end
