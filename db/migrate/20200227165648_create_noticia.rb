class CreateNoticia < ActiveRecord::Migration[6.0]
  def change
    create_table :noticia do |t|
      t.string :title
      t.string :noticiadate
      t.string :summary
      t.string :text
      t.string :spotlight
      t.string :active 

      t.timestamps
    end
  end
end
