class AddAttributesToCandidatos < ActiveRecord::Migration[6.0]
  def change
    add_column :candidatos, :user, :integer
    add_column :candidatos, :birthdate, :date
    add_column :candidatos, :bi, :integer
    add_column :candidatos, :professionalarea, :string
    add_column :candidatos, :habilitationlevel, :string
    add_column :candidatos, :literaryabilities, :string
    add_column :candidatos, :professionalsituation, :string
    add_column :candidatos, :professionalexperience, :string
  end
end
