class ChangeOfertasTableName < ActiveRecord::Migration[6.0]
  def change
    rename_table :oferta, :offers
  end
end
