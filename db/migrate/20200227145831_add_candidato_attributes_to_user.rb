class AddCandidatoAttributesToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :birthdate, :date
    add_column :users, :bi, :integer
    add_column :users, :professionalarea, :string
    add_column :users, :habilitationlevel, :string
    add_column :users, :literaryabilities, :string
    add_column :users, :professionalsituation, :string
    add_column :users, :professionalexperience, :string
  end
end
