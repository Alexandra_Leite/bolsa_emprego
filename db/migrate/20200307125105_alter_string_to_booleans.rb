class AlterStringToBooleans < ActiveRecord::Migration[6.0]
  def change
    change_column :news, :spotlight, :boolean
    change_column :news, :active, :boolean
    change_column :offers, :active, :boolean
  end
end
