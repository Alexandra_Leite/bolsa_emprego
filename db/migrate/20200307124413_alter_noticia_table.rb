class AlterNoticiaTable < ActiveRecord::Migration[6.0]
  def change
    rename_table :noticia, :news
  end
end
