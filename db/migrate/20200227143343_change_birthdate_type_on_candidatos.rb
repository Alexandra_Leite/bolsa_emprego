class ChangeBirthdateTypeOnCandidatos < ActiveRecord::Migration[6.0]
  def change
    change_column :candidatos, :birthdate, :string
  end
end
