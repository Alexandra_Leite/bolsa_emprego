class CreateOferta < ActiveRecord::Migration[6.0]
  def change
    create_table :oferta do |t|
      t.string :title
      t.string :validefrom
      t.string :valideto
      t.string :description
      t.integer :entity
      t.string :professionalactivity
      t.string :contracttype
      t.string :active 
      t.string :salary

      t.timestamps
    end
  end
end
