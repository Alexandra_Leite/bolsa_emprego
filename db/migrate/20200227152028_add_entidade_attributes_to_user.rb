class AddEntidadeAttributesToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :nif, :integer
    add_column :users, :professionalactivity, :string
  end
end
