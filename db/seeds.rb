# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

admin = User.create({ name: 'Admin',
                            email: 'admin@admin.com',
                            password: 'adminadmin',
                            password_confirmation: 'adminadmin',
                            address: 'Avenidade hehehe',
                            postal_code: '6666-666',
                            locality: 'Hell',
                            contacts: '999666999',
                            presentation: 'Muah ah ah ah',
                            page: 'www.welele.com',
                            admin: true
                            })

welele = Candidato.create({ name: 'Welele Welelu',
                            email: 'welele@welele.com',
                            password: 'obuobu',
                            password_confirmation: 'obuobu',
                            address: 'Avenidade hehehe',
                            postal_code: '6666-666',
                            locality: 'Hell',
                            contacts: '999666999',
                            presentation: 'Muah ah ah ah',
                            page: 'www.welele.com',
                            birthdate: '01/02/2003',
                            bi: '66699666',
                            professionalarea: 'Informática',
                            habilitationlevel: 'Secundário',
                            literaryabilities: 'Ler e escrever',
                            professionalsituation: 'Desempregado',
                            professionalexperience: 'Nada de nadinha'
                            })

obu = Entidade.create({ name: 'Obu Obu',
                        email: 'obu@obu.com',
                        password: 'welele',
                        password_confirmation: 'welele',
                        address: 'Rua das bolachas',
                        postal_code: '1234-123',
                        locality: 'Pastelaria',
                        contacts: '987654321',
                        presentation: 'Trolhas 2.0',
                        page: 'www.obuobu.com',
                        nif: '111222333',
                        professionalactivity: 'Engenharia Civil'
                        })