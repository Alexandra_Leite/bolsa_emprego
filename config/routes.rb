Rails.application.routes.draw do
  root 'global_pages#home'
  get '/home', to: 'global_pages#home'

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  get '/edit/password', to: 'password#edit'

  get '/signup', to: 'global_pages#signup'
  get '/signupcandidato', to:'candidatos#new'
  get '/signupentidade', to:'entidades#new'
  get '/new/offer', to:'offers#new'
  get 'new/news', to:'news#new'

  get '/candidatos', to:'candidatos#index'
  get '/entidades', to: 'entidades#index'
  get '/offers', to: 'offers#index'
  get '/news', to: 'news#index'
  
  resources :candidatos do
    member do
      get :offerings, :offerends
    end
  end

  resources :users do
    member do
      get :following, :followers
    end
  end

  resources :users, :entidades, :offers, :news
  resources :candidaturas, only: [:create, :destroy]
  resources :relationships, only: [:create, :destroy]

  #BACKOFFICE
  get '/backoffice', to: 'backoffice#home'
  get '/backoffice/users', to: 'backoffice#index_users'
  get '/backoffice/news', to: 'backoffice#index_news'
  get '/backoffice/users/:id', to: 'backoffice#show_users', as: 'backoffice_show_users'
  get '/backoffice/news/:id', to: 'backoffice#show_news', as: 'backoffice_show_news'
  get '/backoffice/news/:id/edit', to:'backoffice#edit_news', as: 'backoffice_edit_news'
  patch '/backoffice/news/:id/edit', to:'news#update'
  get '/backoffice/users/:id/edit', to:'backoffice#edit_users', as: 'backoffice_edit_users'
  get '/backoffice/users/:id/edit_password', to:'backoffice#edit_user_password', as: 'backoffice_edit_user_password'
  patch '/backoffice/users/:id/edit_password', to:'users#update'
  patch '/backoffice/users/:id/edit', to:'users#update'
  post '/backoffice/new', to:'users#create'
  resources :backoffice
  
end
