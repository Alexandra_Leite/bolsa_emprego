require 'test_helper'

class EntidadesControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get entidades_new_url
    assert_response :success
  end

  test "should get edit" do
    get entidades_edit_url
    assert_response :success
  end

  test "should get show" do
    get entidades_show_url
    assert_response :success
  end

end
