require 'test_helper'

class CandidatosControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get candidatos_new_url
    assert_response :success
  end

  test "should get edit" do
    get candidatos_edit_url
    assert_response :success
  end

end
