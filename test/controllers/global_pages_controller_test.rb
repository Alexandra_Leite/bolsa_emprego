require 'test_helper'

class GlobalPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get global_pages_home_url
    assert_response :success
    assert_select "title", "Bolsa de Emprego"
  end

end
