module EntidadesHelper

    # Returns the Gravatar for the given entidade.
    def gravatar_for(entidade, size: 80)
        gravatar_id = Digest::MD5::hexdigest(entidade.email.downcase)
        gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
        image_tag(gravatar_url, alt: entidade.name, class: "gravatar")
    end
end
