module CandidatosHelper

    # Returns the Gravatar for the given candidato.
    def gravatar_for_candidato(candidato, size: 80)
        gravatar_id = Digest::MD5::hexdigest(candidato.email.downcase)
        gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
        image_tag(gravatar_url, alt: candidato.name, class: "gravatar")
    end
end
