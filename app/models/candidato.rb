class Candidato < User
    has_many :active_candidaturas, class_name: "Candidatura",
                            foreign_key: "candidato_id",
                            dependent: :destroy
    
    has_many :passive_candidaturas, class_name: "Candidatura",
                            foreign_key: "oferta_id",
                            dependent: :destroy

    has_many :offerings, through: :active_candidaturas, source: :oferta
    has_many :offerends, through: :passive_candidaturas, source: :candidato

    scope :search, -> (search) {where("name like ? OR presentation like ?","#{search}%","%#{search}%")}
    scope :locality, -> (local) { where locality: local}
    scope :professionalsituation, -> (situation) { where professionalsituation: situation}
    scope :professionalarea, -> (area) { where professionalarea: area}

    validates :birthdate, presence: true
    VALIDATE_NUMBER = /\A[+-]?\d+\z/i
    validates :bi, format: { with: VALIDATE_NUMBER },
                   length: { minimum: 8, maximum: 8 }
    validates :professionalarea, presence: true
    validates :habilitationlevel, presence: true
    validates :literaryabilities, presence: true
    validates :professionalsituation, presence: true
    validates :professionalexperience, presence: true
    #validates :cv, presence: true

    # Follows a offer.
    def follow_offer(offer)
        active_candidaturas.create(oferta_id: offer.id)
    end

    # Unfollows a offer.
    def unfollow_offer(offer)
        active_candidaturas.find_by(oferta_id: offer.id).destroy
    end

    def offerings?(offer)
        offerings.include?(offer)
    end
end
