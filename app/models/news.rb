class News < ApplicationRecord
    default_scope -> { order(destaque: :desc) }
    has_one_attached :image

    validates :title, presence: true
    validates :noticiadate, presence: true
    validates :summary, presence: true, length: {maximum: 140}
    validates :text, presence: true
    validates :spotlight, presence: true
    validates :active, presence: true

    validates :image, content_type: { in: %w[image/jpeg image/gif image/png],
                                      message: "must be a valid image format" },
                      size: { less_than: 5.megabytes,
                              message: "should be less than 5MB" }

    scope :title, -> (search) { where('title like ?', "%#{search}%")}


    def activate
        update_attribute(:active, true)
    end
    
    def deactivate
        update_attribute(:active, false)
    end
    
    def spotlights
        update_attribute(:spotlight, true)
    end
    
    def not_spotlight
        update_attribute(:spotlight, false)
    end

    # Returns a resized image for display.
    def display_image
        image.variant(resize_to_limit: [300, 300])
    end

    def display_image_mini
        image.variant(resize_to_limit: [150, 150])
    end
end
