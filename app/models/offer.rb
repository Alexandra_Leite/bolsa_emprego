class Offer < ApplicationRecord
    belongs_to :entidade
    has_one_attached :image
    
    has_many :passive_candidaturas, class_name: "Candidatura",
                            foreign_key: "oferta_id",
                            dependent: :destroy

    has_many :offerends, through: :passive_candidaturas, source: :candidato
    
    default_scope -> { order(created_at: :desc) }

    validates :title, presence: true
    validates :validefrom, presence: true
    validates :valideto, presence: true
    validates :description, presence: true
    validates :entidade_id, presence: true
    validates :professionalactivity, presence: true
    validates :contracttype, presence: true
    validates :salary, presence: true

    validates :image, content_type: { in: %w[image/jpeg image/gif image/png],
                                      message: "must be a valid image format" },
                      size: { less_than: 5.megabytes,
                              message: "should be less than 5MB" }

    scope :active, -> { where active: true}
    scope :search, -> (search) {where "title like ? ","%#{search}%"}                  
    scope :locality, -> (local) { where locality: local}
    scope :professionalactivity, -> (area) { where professionalactivity: area}

    # Returns a resized image for display.
    def display_image
        image.variant(resize_to_limit: [300, 300])
    end

    def display_image_mini
        image.variant(resize_to_limit: [150, 150])
    end

    def activate
        update_attribute(:active, true)
    end
    
    def deactivate
        update_attribute(:active, false)
    end
end
