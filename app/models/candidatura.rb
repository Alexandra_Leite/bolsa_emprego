class Candidatura < ApplicationRecord
    belongs_to :candidato, class_name: "Candidato"
    belongs_to :oferta, class_name: "Offer"
    validates :candidato_id, presence: true
    validates :oferta_id, presence: true
end
