class Entidade < User
    has_many :offers, dependent: :destroy

    scope :search, -> (search) {where("name like ? OR presentation like ?","#{search}%","%#{search}%")}
    scope :locality, -> (local) { where locality: local}
    scope :professionalactivity, -> (area) { where professionalactivity: area}

    VALIDATE_NUMBER = /\A[+-]?\d+\z/i
    validates :nif, format: { with: VALIDATE_NUMBER },
                   length: { minimum: 9, maximum: 9 }
    validates :professionalactivity, presence: true
end
