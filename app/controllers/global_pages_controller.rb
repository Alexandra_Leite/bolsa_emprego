class GlobalPagesController < ApplicationController
  def home
    @ent = Entidade.all.order(created_at: :desc)
    @can = Candidato.all.order(created_at: :desc)
    @entidades = @ent.limit(3)
    @candidatos = @can.limit(3)
    @news = News.all.limit(3).order(updated_at: :desc)
    @offers = Offer.all.limit(3).order(created_at: :desc)
  end
end