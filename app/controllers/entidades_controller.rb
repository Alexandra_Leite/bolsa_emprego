class EntidadesController < ApplicationController

  def index
    @entidades = Entidade.order(created_at: :desc)
    filtering_params(params).each do |key, value|
      @entidades = @entidades.public_send(key,value) if value.present?
    end
    @entidades = @entidades.paginate(page: params[:page], per_page:8)
  end

  def show
    @entidade = Entidade.find(params[:id])
    @offers = @entidade.offers.paginate(page: params[:page])
    @followers = @entidade.followers.paginate(page:params[:page], per_page:2)
  end

  def new
    @entidade = Entidade.new
  end

  def edit
    @entidade = Entidade.find(params[:id])
  end

  def create 
    @entidade = Entidade.new(entidade_params)
    if @entidade.save
      log_in @entidade
      redirect_to @entidade
    else
      render 'new'
    end
  end

  def update
    @entidade = Entidade.find(params[:id])
    if @entidade.update(entidade_params)
      redirect_to @entidade
    elsif @entidade.update(password_params)
      redirect_to @entidade
    else
      render 'edit'
    end
  end

  private 

    def entidade_params
      params.require(:entidade).permit(
        :email,
        :name,
        :password,
        :password_confirmation,
        #:photo,
        :address,
        :postal_code,
        :locality,
        :contacts,
        :page,
        :presentation,
        :nif,
        :professionalactivity
      )
    end

    def password_params
      params.require(:entidade).permit(
        :password,
        :password_confirmation
      )
    end

    #filters parameters from params
    def filtering_params(params)
      params.slice(:search,:professionalactivity,:locality)
    end
end
