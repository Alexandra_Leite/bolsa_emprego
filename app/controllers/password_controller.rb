class PasswordController < ApplicationController

  def update
    if params[:user][:password].empty?
      @user.errors.add(:password, "Não pode ser vazia!")
      render 'edit'
    elsif @user&.authenticate(params[:user][:password_current])
      if @user.update(user_params)
        log_in @user
        flash[:success] = "Password foi alterada!"
        redirect_to @user
      else
        render 'edit'
      end
    else
      @user.errors.add(:password, "Password atual incorreta!")
      render 'edit'
    end
  end

  def edit
  end

  private

    def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end

end
