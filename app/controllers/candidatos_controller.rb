class CandidatosController < ApplicationController

  def index
    @candidatos = Candidato.order(created_at: :desc)
    filtering_params(params).each do |key, value|
      @candidatos = @candidatos.public_send(key,value) if value.present?
    end
    @candidatos = @candidatos.paginate(page: params[:page], per_page:8)
  end

  def show
    @candidato = Candidato.find(params[:id])
    @offers_candidature = @candidato.offerings.active.paginate(page:params[:page], per_page:2)
    @followers = @candidato.followers.paginate(page:params[:page], per_page:2)
  end

  def new
    @candidato = Candidato.new
  end

  def edit
    @candidato = Candidato.find(params[:id])
  end

  def update
    @candidato = Candidato.find(params[:id])
    if @candidato.update(candidato_params) || @candidato.update(password_params)
      redirect_to @candidato
    else
      render 'edit'
    end
  end

  def create 
    @candidato = Candidato.new(candidato_params)
    if @candidato.save
      log_in @candidato
      redirect_to @candidato
    else
      render 'new'
    end
  end

  private 

    def candidato_params
      params.require(:candidato).permit(
        :email,
        :name,
        :password,
        :password_confirmation,
        #:photo,
        :address,
        :postal_code,
        :locality,
        :contacts,
        :page,
        :presentation,
        :birthdate,
        :bi,
        #:cv,
        :professionalarea,
        :habilitationlevel,
        :literaryabilities,
        :professionalsituation,
        :professionalexperience
      )
    end

    def password_params
      params.require(:entidade).permit(
        :password,
        :password_confirmation
      )
    end

    #filters parameters from params
    def filtering_params(params)
      params.slice(:search,:professionalarea,:professionalsituation,:locality)
    end

end
