class NewsController < ApplicationController

  def index
    @news_all = News.all.order(updated_at: :desc)
    @news_all = @news_all.paginate(page: params[:page], per_page: 10)  
  end

  def show
    @news_some = News.find(params[:id])
  end
    
  def new
    @news = News.new
  end
    
  def edit
  end
    
  def create 
    @news = News.new(noticia_params)
    @news.image.attach(params[:news][:image])
    if @news.save
      redirect_to @news
    else
      render 'new'
    end
  end
    
  private 
    
    def noticia_params
      params.require(:news).permit(
        :title,
        :noticiadate,
        :summary,
        :text,
        :spotlight,
        :active
      )
    end
end
