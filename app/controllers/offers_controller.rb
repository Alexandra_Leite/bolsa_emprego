class OffersController < ApplicationController

  def index
    @offers = Offer.active.order(created_at: :desc)
    filtering_params(params).each do |key, value|
      @offers = @offers.public_send(key,value) if value.present?
    end
    @offers =  @offers.paginate(page:params[:page], per_page: 8)
  end

  def show
    @offer = Offer.find(params[:id])
    @outras_ofertas = Offer.all.where('professionalactivity like ? AND id != ?', @offer.professionalactivity, @offer.id).limit(2)
    @offer_cand = @offer.offerends.limit(2)
  end
    
  def new
    @offer = Offer.new
  end
    
  def edit
    @offer = Offer.find(params[:id])
  end
    
  def create 
    @offer = current_user.offers.build(oferta_params)
    @offer.image.attach(params[:offer][:image])
    if @offer.save
      redirect_to @offer
    else
      render 'new'
    end
  end

  def update
    @offer = Offer.find(params[:id])
    if @offer.update(oferta_params)
      redirect_to @offer
    else
      render 'edit'
    end
  end

  def deactivate
    @offer = Offer.find(params[:id])
    @entidade = Entidade.find(@offer.entidade_id)
    @offer.deactivate
    redirect_to @entidade
  end

  def activate
    @offer = Offer.find(params[:id])
    @entidade = Entidade.find(@offer.entidade_id)
    @offer.activate
    redirect_to @entidade
  end

  def destroy
    current_user.offers.find(params[:id]).destroy
    flash[:success] = "Oferta Cancelada!"
    redirect_to current_user
  end

  private 
    
    def oferta_params
      params.require(:offer).permit(
        :title,
        :validefrom,
        :valideto,
        :description,
        :image,
        :professionalactivity,
        :contracttype,
        :active,
        :salary
      )
    end

    def filtering_params(params)
      params.slice(:search,:locality,:professionalactivity)
    end
end
