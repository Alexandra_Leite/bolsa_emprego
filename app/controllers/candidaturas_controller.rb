class CandidaturasController < ApplicationController
    before_action :logged_in_user

    def create
        offer = Offer.find(params[:oferta_id])
        current_user.follow_offer(offer)
        redirect_to offer
    end

    def destroy 
        offer = Candidatura.find(params[:id]).oferta
        current_user.unfollow_offer(offer)
        redirect_to offer
    end
end
